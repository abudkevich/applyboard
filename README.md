This Project consist an IaC Terraform files for Applyboard test task.

Issued data:
1. Create dev environment in one AWS region with EKS cluster which will consist:
 - one front-end container (Web service on Ruby is preferrable)
 - one back-end container (any service on Ruby is prefferable)
 - one mysql db container (for previous back-end services)  

 Note: front-end container must be linked with back-end container, 
 back-end container must be linked with mysql db container.
 
2. Create prod environment in the second AWS region with EKS cluster which will consist:
 - one front-end container (Web service on Ruby is preferrable)
 - one back-end container (any service on Ruby is prefferable)
 - RDS DB  
 
 Note: front-end container must be linked with back-end container.

=========================================================================================

For launch Terraform (plan, apply and destroy) we will use AWS credentials
in the command line. Examples for plan and apply commands:

```
terraform plan \
-var access_key=<your AWS key> \
-var secret_key=<your AWS secret key> -out out.plan
```

```
terraform apply \
-var access_key=<your AWS key> \
-var secret_key=<your AWS secret key>
```