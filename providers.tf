#
# Provider Configuration
#

provider "aws" {
  access_key = "{}"
  secret_key = "{}"
}

module "cluster-dev" {
  source = "dev"
}

module "cluster-prod" {
  source = "prod"
  vpc_id = "${module.cluster-prod.vpc_id}"
}
